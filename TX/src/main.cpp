#include <Freenove_WS2812_Lib_for_ESP32.h>
#include <LoRa.h>
#include <SPI.h>
#include <U8g2lib.h>
#include <oled_font.h>

//项目
#define ProjectName  "LoRa Receiver"

//Lora初始化 
#define ss 5
#define rst 14
#define dio0 2
#define BAND 433E6//lora信号 433MHz

//ESP32使用这个U8G2初始化
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2( /* 转180*/U8G2_R2, 
                                          /* reset=*/U8X8_PIN_NONE, 
                                          /* clock=*/22,
                                          /* data=*/21); //SCL:D22 SDA:D21

//WS2812
#define LEDS_COUNT    2     //WS2812个数
#define LEDS_PIN      26    //使用GPIO 26
#define CHANNEL       0     //ESP定时器通道0
#define WS2812_1      0     //第1个WS2812
#define WS2812_2      1     //第2个WS2812
Freenove_ESP32_WS2812 WS2812 = Freenove_ESP32_WS2812(LEDS_COUNT, LEDS_PIN, CHANNEL, TYPE_GRB);

int counter = 0;
 
void setup() 
{
  Serial.begin(115200);         //串口波特率115200 
  u8g2.begin();                 //OLED初始化
  u8g2.enableUTF8Print();       // 使print支持UTF8字集

  WS2812.begin();               //WS2812初始化
  WS2812.setBrightness(15);     //设置WS2812亮度

  while (!Serial);              //等待串口初始化完成
  Serial.printf("\r\nProject: %s\r\n",ProjectName); 
  
  LoRa.setPins(ss, rst, dio0);  //设置lora引脚

  /*U8G2绘制ESPLoRa图标*/
  u8g2.firstPage();
  do{
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawXBMP(0,8, bmp1_x, bmp1_y, bmp1);
  } while ( u8g2.nextPage() );
  delay(5000);
  

  /*LoRa初始化*/
  while (!LoRa.begin(BAND))
  {
    Serial.println(".");
    delay(500);
  }
    Serial.printf("LoRa Initializing OK!\r\n");
  
  //--------这里是通信的信道，------ 
  LoRa.setSyncWord(0xA5);//接受来自信道0xA5的信号消息  发送和接收的信道要一直。不写的话就是接收所有的数据包！
  LoRa.setTxPower(20);

  u8g2.clearDisplay();

}
 
void loop() 
{
  u8g2.setFont(u8g2_font_t0_17_tr);//u8g2字体

  Serial.printf("Sending packet: %d\r\n",counter);

  u8g2.setCursor(0,20);
  u8g2.print("Packet:");
 
  LoRa.beginPacket();   //发送数据包
  LoRa.print("hello ");
  LoRa.print(counter);

  u8g2.setCursor(50,40);
  u8g2.print(counter);

  u8g2.sendBuffer();//显示
  LoRa.endPacket();//终止接收包
 
  counter++;
  u8g2.clearBuffer();
  delay(5000);

  WS2812.setLedColor(0,122,0,138);  //第一颗WS2812
  WS2812.setLedColor(1,176,188,0);  //第二颗WS2812



}
